import sys
import threading
import time
from PyQt5 import uic
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *


""" VARIABLES GLOBALES """

#Variables generales
total_clicks = 0
taza_x_seg = 5

#Multiplicadores
multiplicadorx5 = 5
multiplicadorx10 = 10

#Indices
_indicex5 = 0
_indicex10 = 0
_indiceVagox1 = 0
_indiceVagox10 = 0
_indiceVagox100 = 0


#Costos de recompensas
_costo_Vagox1 = 1000
_costo_Vagox10 = 30000
_costo_Vagox100 = 50000
_costo_reset = 1000000000
costo_x5 = 40
costo_x10 = 80

#Autoincrementables
autoincrementablex1 = 0
autoincrementablex10 = 0
autoincrementablex100 = 0

#Variables Esteticas
desactivado = " background-color:'grey' "
activado    = "background-color: #ff6d24 " 




class Clicker(QMainWindow):

    """ Hilos para procesos en segundo plano """
    
    def hilo1(self):
        print("--== Hilo 1 Trabajando ==--")
        self.hilo_gui = threading.Thread(target=self.comprobar, daemon=True)
        self.hilo_gui.start()

    def hilo2(self):
        print("--== Hilo 2 Trabajando ==--")
        self.hilo2_gui = threading.Thread(target=self.vagox1, daemon=True)
        self.hilo2_gui.start()
    
    def hilo3(self):
        print("--== Hilo 3 Trabajando ==--")
        self.hilo3_gui = threading.Thread(target=self.vagox10, daemon=True)
        self.hilo3_gui.start()

    def hilo4(self):
        print("--== Hilo 4 Trabajando ==--")
        self.hilo4_gui = threading.Thread(target=self.vagox100, daemon=True)
        self.hilo4_gui.start()

    def hilo_progressbar_total(self):
        print("Hilo general progressbar resets trabajando")
        self.progress_bar_general = threading.Thread(target=self.general_progress, daemon=True)
        self.progress_bar_general.start()


    def general_progress(self):
        global total_clicks
        while True:
            self.barra_progreso.setValue(total_clicks)
            time.sleep(1)


    def comprobar(self):

        while True:
            if total_clicks >= costo_x5:
                self.btn_clickx5.setEnabled(True)
                self.btn_clickx5.setStyleSheet( activado )
            else:
                self.btn_clickx5.setEnabled(False)
                self.btn_clickx5.setStyleSheet( desactivado )


            if total_clicks >= costo_x10:
                self.btn_clickx10.setEnabled(True)
                self.btn_clickx10.setStyleSheet(activado)
            else:
                self.btn_clickx10.setEnabled(False)   
                self.btn_clickx10.setStyleSheet(desactivado)
                

            if total_clicks >= _costo_Vagox1:
                self.btn_recompensa1.setEnabled(True)
                self.btn_recompensa1.setStyleSheet(activado)
            else:
                self.btn_recompensa1.setEnabled(False)
                self.btn_recompensa1.setStyleSheet(desactivado)

            if total_clicks >= _costo_Vagox10:
                self.btn_recompensa3.setEnabled(True)
                self.btn_recompensa3.setStyleSheet(activado)
            else:
                self.btn_recompensa3.setEnabled(False)
                self.btn_recompensa3.setStyleSheet(desactivado)

            if total_clicks >= _costo_Vagox100:
                self.btn_recompensa4.setEnabled(True)
                self.btn_recompensa4.setStyleSheet(activado)
            else:
                self.btn_recompensa4.setEnabled(False)
                self.btn_recompensa4.setStyleSheet(desactivado)



            if total_clicks < 1000:
                self.lbl_clicks.setText( str(total_clicks) )

            elif total_clicks >= 1000 and total_clicks <= 1000000:
                self.lbl_clicks.setText( str( round( (total_clicks/1000) ) ) + "K" )

            elif total_clicks > 1000000 and total_clicks <= 1000000000:
                self.lbl_clicks.setText( str( round( (total_clicks/1000000) ) ) + "M" )
            
            elif total_clicks > 1000000000 and total_clicks <= 1000000000000:
                self.lbl_clicks.setText( str( round( (total_clicks/1000000000) ) ) + "B" )
            

            time.sleep(0.5)         

    def ejecutar_hilo(self):
        self.hilo2()

    def ejecutar_hilo3(self):
        self.hilo3()
    
    def ejecutar_hilo4(self):
        self.hilo4()

    def vagox1(self):
        global _indiceVagox1
        global _costo_Vagox1
        global total_clicks
        global autoincrementablex1
        global taza_x_seg

        
        total_clicks = 0
        _indiceVagox1 += 1


        while True:
            autoincrementablex1 += 1
            total_clicks += autoincrementablex1

            if _indiceVagox1 > 0:
                total_clicks += (autoincrementablex1 * taza_x_seg)
                self.label.setText("Soy Vago x{}".format( str( _indiceVagox1 ) ))

            time.sleep(1)
            

    def vagox10(self):
        global _indiceVagox10
        global _costo_Vagox10
        global total_clicks
        global autoincrementablex10
        global taza_x_seg

        total_clicks = 0
        _indiceVagox10 += 1
        
        while True:
            autoincrementablex10 += 1
            total_clicks += autoincrementablex10
            if _indiceVagox1 > 0:
                total_clicks += ( (autoincrementablex10 * taza_x_seg) * 10)
                self.label_4.setText("Soy Vago x{}".format( str( _indiceVagox10 * 10 ) ))

            time.sleep(1)
          
    def vagox100(self):
        global _indiceVagox100
        global _costo_Vagox100
        global total_clicks
        global autoincrementablex100
        global taza_x_seg

        total_clicks = 0
        _indiceVagox100 += 1
        
        while True:
            autoincrementablex100 += 1
            total_clicks += autoincrementablex100
            if _indiceVagox1 > 0:
                total_clicks += ( (autoincrementablex100 * taza_x_seg) * 100)
                self.label_3.setText("Soy Vago x{}".format( str( _indiceVagox100 * 100 ) ))

            time.sleep(1)
           
    def __init__(self):

        super(QMainWindow, self).__init__()
        uic.loadUi("interfaz.ui", self)

        self.btn_click.clicked.connect(lambda: self.click(0))

        #Desactivamos los botones
        self.btn_clickx5.setEnabled(False)
        self.btn_clickx10.setEnabled(False)
        self.btn_recompensa1.setEnabled(False)
        self.btn_recompensa2.setEnabled(False)
        self.btn_recompensa3.setEnabled(False)
        self.btn_recompensa4.setEnabled(False)
        self.btn_resetear_progreso.setEnabled(False)


        #Estilos botones
        self.btn_clickx5.setStyleSheet( desactivado )
        self.btn_clickx10.setStyleSheet( desactivado )
        self.btn_recompensa1.setStyleSheet( desactivado )
        self.btn_recompensa2.setStyleSheet( desactivado )
        self.btn_recompensa3.setStyleSheet( desactivado )
        self.btn_recompensa4.setStyleSheet( desactivado )
        self.btn_resetear_progreso.setStyleSheet( desactivado )

        self.btn_clickx5.clicked.connect( self.aumentox5 )
        self.btn_clickx10.clicked.connect(self.aumentox10)

        self.btn_recompensa1.clicked.connect(self.ejecutar_hilo)
        #self.btn_recompensa2.clicked.connect()
        self.btn_recompensa3.clicked.connect(self.ejecutar_hilo3)
        self.btn_recompensa4.clicked.connect(self.ejecutar_hilo4)


        #Seteamos la barra de progreso general
        self.barra_progreso.setMaximum(_costo_reset)
        self.btn_resetear_progreso.setText( "Resetear (Costo {})".format( _costo_reset ) )

        #Seteamos textos de botones
        self.btn_clickx5.setText( "Tazas +5 (Costo: {})".format(costo_x5) )
        self.btn_clickx10.setText( "Tazas +10 (Costo: {})".format(costo_x10) )
        self.btn_recompensa1.setText( "Recompensa1 (Costo: {})".format( _costo_Vagox1 ) )
        self.btn_recompensa3.setText( "Recompensa2 (Costo: {})".format(_costo_Vagox10) )
        self.btn_recompensa4.setText( "Recompensa4 (Costo: {})".format(_costo_Vagox100) )

        
        #Llamamos a un hilo para que este siempre a la escucha
        self.hilo1()
        self.hilo_progressbar_total()

    def obtener_1_taza(self):
        global total_clicks
        total_clicks += 1
        self.lbl_clicks.setText( str(total_clicks) )  

    def click(self, valor): 
        global total_clicks
        global _indicex5
        global _indicex10
        global costo_x5
        global costo_x10
        global multiplicadorx5
        global clicks

        if _indicex5 <= 0 and _indicex10 <= 0:
            self.obtener_1_taza()

        if _indicex5 > 0:
            total_clicks += ( multiplicadorx5 * _indicex5 ) 
            self.lbl_clicks.setText(str(total_clicks))

        if _indicex10 > 0:
            total_clicks += (multiplicadorx10 * _indicex10) 
            self.lbl_clicks.setText(str(total_clicks))

    def DEBUG(self, a=0, b=0, c=0, d=0):
        global total_clicks
        global multiplicadorx10
        global multiplicadorx5
        global _indicex5
        print("======= DEBUG ======= ")
        print("Esto vale el parametro 1: ", a)
        print("Esto vale el paramentro 2: ", b)
        print("Esto vale el paramentro 3: ", c)
        print("Esto vale el paramentro 4: ", d)
        print("Esto vale multiplicadorx5: ", multiplicadorx5)
        print("Esto vale multiplicadorx10: ", multiplicadorx10)

    def aumentox5(self):
        global _indicex5
        global costo_x5
        global total_clicks

        total_clicks = total_clicks - costo_x5
        _indicex5 += 1
        self.lbl_multiplicadorx5.setText("Total Multiplicador: {}".format( str(_indicex5 * 5) ))
        self.lbl_clicks.setText( self.replace_text( str(total_clicks)[0:3], str(total_clicks)[4:-1] ) )
        self.btn_clickx5.setEnabled(False)

    def aumentox10(self):
        global _indicex10
        global costo_x10
        global total_clicks

        total_clicks = total_clicks - costo_x10
        _indicex10 += 1
        self.lbl_multiplicadorx10.setText( "Total Multiplicador: {}".format( str(_indicex10 * 10) ) )
        self.lbl_clicks.setText(str(total_clicks))
        self.btn_clickx10.setEnabled(False)


        



if __name__ == "__main__":
    app = QApplication(sys.argv)
    gui = Clicker()
    gui.show()
    sys.exit(app.exec_())
